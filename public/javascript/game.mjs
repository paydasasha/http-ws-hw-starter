const username = sessionStorage.getItem("username");
const roomsPage = document.getElementById("rooms-page");
const createRoomBtn = document.getElementById('add-room-btn');
const roomContainer = document.getElementById('room-container');
const gamePage = document.getElementById('game-page');
const leaveRoomButton = document.getElementById('quit-room-btn');
const gamePageHeaderSpan = document.getElementById('gamePageHeaderSpan');
const headerUserName = document.getElementById('headerUserName');

if (!username) {
  window.location.replace("/login");
}

headerUserName.innerText = `user: ${username}`;

const socket = io("localhost:3002", {
  query: {
    username
  }
});

const userAllreadyExist = function (username) {
  window.alert(`User ${username} is allready in system`);
  window.sessionStorage.clear();
  window.location.replace("/login");
};
const clearStorage = function () {
  window.Storage.clear();
  window.location.replace("/login");
}

const onCreateNewRoom = function () {
  let roomName = prompt('Enter Room Name');
  if (roomName) {
    socket.emit('CREATE_ROOM', roomName);
  }
};


const joinRoom = function (roomId) {
  socket.emit('JOIN_ROOM', roomId);
};

const onLeaveRoom = function () {
  socket.emit('LEAVE_ROOM', gamePageHeaderSpan.innerText);
  console.log(gamePageHeaderSpan.innerText);
};
const leaveByConnection = function () {
  socket.emit('LEAVE_ROOM', gamePageHeaderSpan.innerText);
};

createRoomBtn.addEventListener('click', onCreateNewRoom);
leaveRoomButton.addEventListener('click', onLeaveRoom);

const roomNameNotAvailable = function (nameId) {
  alert(`Name of Room ${nameId} not available`);
}

const changeUsersList = function (currentRoom) {
  const usersList = document.getElementById('usersList');
  usersList.innerHTML = '';
  console.log(currentRoom);
  currentRoom.users.map(user => {
    let userItem = document.createElement('li');
    let statusCircle = document.createElement('div');
    let nameSpan = document.createElement('span');
    let progressBar = document.createElement('progress');
    progressBar.classList.add(`user-progress`, `${username}`);
    progressBar.setAttribute('max', '100');
    progressBar.setAttribute('value', `${user.progress}`);
    if (user.progress === 100) {
      progressBar.classList.add('finished');
      if (user.name === username) {
        window.removeEventListener('keydown', checkKeys);
      };
    };
    userItem.classList.add('userItem');
    nameSpan.innerText = user.name;
    if (user.status === 'NA') {
      statusCircle.className = 'ready-status-red'
    } else if (user.status === 'A') {
      statusCircle.className = 'ready-status-green';
    }
    if (user.name === username) nameSpan.innerText += ' (you)';
    userItem.appendChild(statusCircle);
    userItem.appendChild(nameSpan);
    userItem.appendChild(progressBar);
    usersList.appendChild(userItem);
  });
}

const onGamePage = function (currentRoom) {
  gamePage.classList.remove('display-none');
  roomsPage.classList.add('display-none');
  console.log(`${username} is gaming`);
  gamePageHeaderSpan.innerText = currentRoom.roomName;
  let prevGameContainer = document.getElementById('gameContainer');
  if (!prevGameContainer) {
    let gameContainer = document.createElement('div');
    let textSquare = document.createElement('div');
    let usersList = document.createElement('ul');
    let readyBtn = document.createElement('button');
    let notReadyBtn = document.createElement('button');
    readyBtn.id = 'ready-btn';
    notReadyBtn.id = 'not-ready-btn';
    readyBtn.classList.add('ready-btn');
    notReadyBtn.classList.add('not-ready-btn', 'display-none');
    readyBtn.innerText = 'READY';
    notReadyBtn.innerText = 'NOT READY';
    readyBtn.addEventListener('click', getReady);
    notReadyBtn.addEventListener('click', getNotReady);
    textSquare.appendChild(readyBtn);
    textSquare.appendChild(notReadyBtn);

    let users = currentRoom.users.map(user => {
      let userItem = document.createElement('li');
      let statusCircle = document.createElement('div');
      let nameSpan = document.createElement('span');
      let progressBar = document.createElement('progress');
      progressBar.classList.add(`user-progress`, `${user.name}`);
      userItem.classList.add('userItem');
      nameSpan.innerText = user.name;
      if (user.status === 'NA') {
        statusCircle.className = 'ready-status-red'
      } else {
        statusCircle.className = 'ready-status-green';
      }
      if (user.name === username) nameSpan.innerText += ' (you)';
      userItem.appendChild(statusCircle);
      userItem.appendChild(nameSpan);
      userItem.appendChild(progressBar);
      return userItem;
    });
    usersList.id = 'usersList';
    usersList.appendChild(...users);
    usersList.classList.add('userList');
    gameContainer.appendChild(usersList);
    gameContainer.id = 'gameContainer';
    textSquare.classList.add('textSquare');
    textSquare.id = 'text-container';
    gameContainer.appendChild(textSquare);
    gameContainer.classList.add('gameContainer');
    gamePage.appendChild(gameContainer);
  } else {
    changeUsersList(currentRoom);
  };
};

const getReady = function () {
  let readyBtn = document.getElementById('ready-btn');
  readyBtn.classList.add('display-none');
  let notReadyBtn = document.getElementById('not-ready-btn');
  notReadyBtn.classList.remove('display-none');
  socket.emit('USER_GET_READY', gamePageHeaderSpan.innerText);

};

const getNotReady = function () {
  socket.emit('USER_GET_NOT_READY', gamePageHeaderSpan.innerText);
  let readyBtn = document.getElementById('ready-btn');
  readyBtn.classList.remove('display-none');
  let notReadyBtn = document.getElementById('not-ready-btn');
  notReadyBtn.classList.add('display-none');
}
const startGame = function (seconds) {
  let notReadyBtn = document.getElementById('not-ready-btn');
  notReadyBtn.classList.add('display-none');
  leaveRoomButton.classList.add('display-none');
  countDownBeforeGame(seconds);
};

const countDownBeforeGame = function (seconds) {
  socket.emit('ON_COUNT_DOWN_BEFORE', gamePageHeaderSpan.innerText);
  let countDownBefore = document.createElement('p');
  countDownBefore.classList.add('timer-before');
  let textSquare = document.getElementById('text-container');
  textSquare.appendChild(countDownBefore);
  let timer = setInterval(function () {
    if (seconds <= 0) {
      clearInterval(timer);
      countDownBefore.remove();
      timerWhileGaming();
      if (!document.getElementById('spanContainer')) {
        socket.emit('GET_RANDOM', gamePageHeaderSpan.innerText);
      };
    }
    countDownBefore.innerText = seconds;
    seconds -= 1
  }, 1000);

};

const timerWhileGaming = function () {
  socket.emit('GET_GAME_DURATION');
};

let index = 0;

const checkKeys = function (e) {
  let span = document.getElementById(`span-${index}`);
  let spanAfter = document.getElementById(`span-${index + 1}`);
  // if (span) { 
  if (e.key === span.innerText) {
    span.classList.add('colored-span');
    span.classList.remove('spanAfter');
    if (spanAfter) spanAfter.classList.add('spanAfter');
    let wholeText = document.querySelectorAll('#spanContainer>span').length;
    let percents = ((index + 1) / wholeText) * 100;

    socket.emit('CHANGE_PROGRESSBAR', {
      percents: percents,
      room: gamePageHeaderSpan.innerText
    });
    return index++;
  };

  // };
};

const finishGame = function (roomId) {
  document.getElementById('timerWhile').remove();
  let finishModal = document.createElement('div');
  finishModal.id = 'finishModal';
  let finishPopup = document.createElement('div');
  let header = document.createElement('h2');
  let modalContent = document.createElement('div');
  modalContent.classList.add('modalContent');
  let winnersList = document.createElement('ol');
  roomId.winnersList.map((item, i) => {
    let listItem = document.createElement('li');
    listItem.innerText = `${i+1} - ${item}`;
    listItem.id = `place-${i+1}`;
    winnersList.appendChild(listItem);
  });
  header.innerText = 'RESULTS';
  let closeBtn = document.createElement('a');
  closeBtn.innerText = 'x';
  closeBtn.id = 'quit-results-btn';
  closeBtn.classList.add('close');
  closeBtn.addEventListener('click', closeModal);

  modalContent.appendChild(winnersList);
  finishPopup.appendChild(header);
  finishPopup.appendChild(closeBtn);
  finishPopup.appendChild(modalContent);
  finishPopup.classList.add('popup');
  finishModal.classList.add('finish-modal');
  finishModal.appendChild(finishPopup);
  document.body.appendChild(finishModal);
}

const finishGameByTime = function (roomId) {
  if (!document.getElementById('quit-results-btn')) {
    let finishModal = document.createElement('div');
    finishModal.id = 'finishModal';
    let finishPopup = document.createElement('div');
    let header = document.createElement('h2');
    let modalContent = document.createElement('div');
    modalContent.classList.add('modalContent');
    let winnersList = document.createElement('ol');
    roomId.winnersList.map((item, i) => {
      let listItem = document.createElement('li');
      listItem.innerText = `${i+1} - ${item}`;
      listItem.id = `place-${i+1}`;
      winnersList.appendChild(listItem);
    });
    roomId.users.map(user => {
      if (!roomId.winnersList.includes(user.name)) {
        let listItem = document.createElement('li');
        listItem.innerText = `NOT FINISHED - ${user.name}`;
        listItem.id = `place-NOT FINISHED`;
        winnersList.appendChild(listItem);
      }
    })
    header.innerText = 'RESULTS';
    let closeBtn = document.createElement('a');
    closeBtn.innerText = 'x';
    closeBtn.id = 'quit-results-btn';
    closeBtn.classList.add('close');
    closeBtn.addEventListener('click', closeModal);


    modalContent.appendChild(winnersList);
    finishPopup.appendChild(header);
    finishPopup.appendChild(closeBtn);
    finishPopup.appendChild(modalContent);
    finishPopup.classList.add('popup');
    finishModal.classList.add('finish-modal');
    finishModal.appendChild(finishPopup);
    document.body.appendChild(finishModal);
  };
};

const closeModal = function () {
  document.getElementById('quit-room-btn').classList.remove('display-none');
  document.getElementById('finishModal').remove();
  document.getElementById('spanContainer').remove();
  document.getElementById('ready-btn').classList.remove('display-none');
  index = 0;
  socket.emit('RESET_RANDOM', gamePageHeaderSpan.innerText);
};

function secondsForGame(seconds) {
  let countDownWhile = document.createElement('p');
  countDownWhile.classList.add('timer-while');
  countDownWhile.id = 'timerWhile';
  let textSquare = document.getElementById('text-container');
  textSquare.appendChild(countDownWhile);
  let timer = setInterval(function () {
    if (seconds <= 0) {
      clearInterval(timer);
      countDownWhile.remove();
      socket.emit('TIME_EXPIRED', gamePageHeaderSpan.innerText);
      window.removeEventListener('keydown', checkKeys);
    }
    countDownWhile.innerText = `${seconds} seconds left`;
    seconds -= 1
  }, 1000);
};

const showText = async function (random) {
  let text = await getText(random);
  let spanContainer = document.createElement('div');
  spanContainer.id = 'spanContainer';
  for (let i = 0; i < text.length; i++) {
    let span = document.createElement('span');
    span.innerText = text[i];
    spanContainer.appendChild(span);
    span.id = `span-${i}`;
  }
  let textContainer = document.getElementById('text-container');
  textContainer.appendChild(spanContainer);
  window.addEventListener('keydown', checkKeys);
}

const getText = async function (randomNumb) {
  let textArray = await fetch('http://localhost:3002/game/texts/:id').then(res => res.json());
  let text = await textArray[Math.floor(randomNumb * (textArray.length))]
  return text;
};

// console.log(await getText(random));

const onLeaveGamePage = function (rooms) {
  gamePage.classList.add('display-none');
  roomsPage.classList.remove('display-none');
  console.log(`${username} leaved room`);
  updateRooms(rooms);
};


const updateRooms = function (data) {
  let allRooms = data.rooms;
  let maximumUsersForOneRoom = data.max_users;
  if (allRooms) {
    let rooms = allRooms.filter(function (room) {
      return room.qntOfUsers < maximumUsersForOneRoom
    });
    rooms = rooms.filter(function (room) {
      return room.isOnGame !== true
    });
    const filteredRooms = rooms.map(function (roomItem) {
      let roomItemContainer = document.createElement('div');
      let roomName = document.createElement('p');
      roomName.innerText = roomItem.roomName;
      let qttyOfUsers = document.createElement('p');
      qttyOfUsers.innerText = `Quantity: ${roomItem.qntOfUsers}`;
      let joinButton = document.createElement('button');
      joinButton.innerText = 'Join';
      joinButton.classList.add('join-btn');
      joinButton.id = roomItem.roomName;
      joinButton.addEventListener('click', () => joinRoom(event.target.id));
      roomItemContainer.classList.add('room-item-container');
      roomItemContainer.appendChild(roomName);
      roomItemContainer.appendChild(qttyOfUsers);
      roomItemContainer.appendChild(joinButton);
      return roomItemContainer;
    });
    roomContainer.innerHTML = "";
    roomContainer.append(...filteredRooms);
  };
};


socket.on('USER_ALLREADY_EXIST', userAllreadyExist);

socket.on("UPDATE_ROOMS", updateRooms);
socket.on('ROOM_EXISTS', roomNameNotAvailable);
socket.on('ROOM_JOINED', onGamePage);
socket.on('ROOM_LEFT', onLeaveGamePage);
socket.on('SMBD_ENTERED', changeUsersList);
socket.on('SMBD_LEFT', changeUsersList);
socket.on('USER_GOT_READY', changeUsersList);
socket.on('USER_GOT_NOT_READY', changeUsersList);
socket.on('START_GAME', startGame);
socket.on('GET_SECONDS_FOR_GAME', secondsForGame);
socket.on('SET_RANDOM', await showText);
socket.on('UDATE_PROGRESS_BAR', changeUsersList);
socket.on('FINISH_GAME', finishGame);
socket.on('FINISH_GAME_BY_TIME', finishGameByTime);
socket.on('ON_CURRENT_ROOM', onGamePage);