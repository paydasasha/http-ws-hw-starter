import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME,
  MAXIMUM_USERS_FOR_ONE_ROOM
} from "./config";

let usersList = new Set();
let rooms = new Array();
const getCurrentRoom = function (roomId) {
  let currentRoom = rooms.find(item => item.roomName === roomId);
  return currentRoom;
}
let room = new Array();
let max_users = MAXIMUM_USERS_FOR_ONE_ROOM;
// const counterMap = new Map(rooms.map(room => [roomId, 0]));

// const getCurrentRoomId = socket => Object.keys(socket.rooms).find(room => counterMap.has(room));

export default io => {
  io.on("connection", socket => {

    const username = socket.handshake.query.username;
    let userRoomId = null;

    let user = {
      name: username,
      status: 'NA',
      progress: 0
    };

    if (usersList.has(username)) {
      return socket.emit('USER_ALLREADY_EXIST', username);
    } else {
      usersList.add(username);
      console.log(usersList);
      console.log(`${username} connected`);
    };

    socket.on('GET_GAME_DURATION', () => socket.emit('GET_SECONDS_FOR_GAME', SECONDS_FOR_GAME));

    socket.on('CREATE_ROOM', function (room) {
      if (rooms.find(item => room == item.roomName)) {
        socket.emit('ROOM_EXISTS', room);
      } else {
        socket.join(room);
        rooms.push({
          roomName: room,
          qntOfUsers: Object.keys(io.sockets.adapter.rooms[room].sockets).length,
          users: new Array(),
          winnersList: new Array()
        });
        let currentRoom = getCurrentRoom(room);
        let user = currentRoom.users.find(user => user.name === username);
        user = {
          name: username,
          status: 'NA',
          progress: 0
        };
        currentRoom.users.push(user);
        console.log(currentRoom.users);
        io.to(room).emit('ROOM_JOINED', currentRoom);
        console.log(`${username} entered ${room}`);
        io.emit("UPDATE_ROOMS", {
          rooms: rooms,
          max_users: max_users
        });
        rooms.forEach(element => {
          console.log(`${element.roomName} | ${element.qntOfUsers}`);
        });
      };

    });

    socket.on('SET_CURRENT_ROOM', (roomId) => {
      let currentRoom = getCurrentRoom(roomId);

      socket.emit('ON_CURRENT_ROOM', currentRoom);
    });

    io.emit("UPDATE_ROOMS", {
      rooms: rooms,
      max_users: max_users
    });

    socket.on('TIME_EXPIRED', function (roomId) {
      let currentRoom = getCurrentRoom(roomId);
      io.to(roomId).emit('FINISH_GAME_BY_TIME', currentRoom);
      currentRoom.users = currentRoom.users.map(user => {
        return {
          ...user,
          status: 'NA',
          progress: 0
        }
      });
      currentRoom.random = Math.random();
      io.to(roomId).emit('UDATE_PROGRESS_BAR', currentRoom);
    });

    socket.on('RESET_RANDOM', function (roomId) {
      let currentRoom = getCurrentRoom(roomId);
      currentRoom.random = null;
    });
    socket.on('GET_RANDOM', function (roomId) {
      let currentRoom = getCurrentRoom(roomId);
      if (!currentRoom.random) {
        let random = Math.random();
        currentRoom.random = random;
      }
      console.log(`random = ${currentRoom.random}`);
      io.to(roomId).emit('SET_RANDOM', currentRoom.random);
    });

    socket.on('JOIN_ROOM', function (roomId) {

      socket.join(roomId);
      userRoomId = roomId;
      let currentRoom = getCurrentRoom(roomId);
      let user = currentRoom.users.find(user => user.name === username);
      user = {
        name: username,
        status: 'NA',
        progress: 0
      };
      currentRoom.qntOfUsers = Object.keys(io.sockets.adapter.rooms[roomId].sockets).length;
      currentRoom.users.push(user);
      socket.emit('ROOM_JOINED', currentRoom);
      io.to(roomId).emit('SMBD_ENTERED', currentRoom);
      console.log(currentRoom.users);
      console.log(`${username} entered ${roomId}`);
      rooms.forEach(element => {
        console.log(`${element.roomName} | ${element.qntOfUsers}`);
      });
      io.emit("UPDATE_ROOMS", {
        rooms: rooms,
        max_users: max_users
      });
    });



    socket.on('CHANGE_PROGRESSBAR', function (data) {
      let currentRoom = getCurrentRoom(data.room);
      let percents = data.percents;
      let roomUser = currentRoom.users.find(user => user.name === username);
      roomUser.progress = percents;
      if (percents === 100) {
        if (!currentRoom.winnersList.includes(username)) {
          currentRoom.winnersList.push(username);
        };
        console.log(currentRoom);
      }
      let notFinished = currentRoom.users.filter(user => user.progress !== 100);
      console.log(notFinished);
      if (notFinished.length === 0) {
        io.to(data.room).emit('FINISH_GAME', currentRoom);
        currentRoom.users = currentRoom.users.map(user => {
          return {
            ...user,
            status: 'NA',
            progress: 0
          }
        });
        console.log(currentRoom);
      }
      io.to(data.room).emit('UDATE_PROGRESS_BAR', currentRoom);
    })

    socket.on('LEAVE_ROOM', function leaveRoom(roomId) {
      let currentRoom = getCurrentRoom(roomId);
      let currentUser = currentRoom.users.find(item => item.name === username);
      currentUser.progress = 0;
      currentUser.status = 'NA';

      currentRoom.users = currentRoom.users.filter(item => item.name !== username.toString());
      socket.leave(roomId);
      if (!io.sockets.adapter.rooms[roomId]) {
        rooms = rooms.filter(room => room.roomName !== roomId);
        console.log(rooms);
      } else {
        currentRoom.qntOfUsers = Object.keys(io.sockets.adapter.rooms[roomId].sockets).length;

      }
      console.log(`${username} leaved ${roomId}`);
      io.emit("UPDATE_ROOMS", {
        rooms: rooms,
        max_users: max_users
      });
      socket.emit('ROOM_LEFT', rooms);
      io.to(roomId).emit('SMBD_LEFT', currentRoom);
      let startGame = currentRoom.users.map(user => user.status);
      if (!startGame.includes('NA')) {
        io.to(roomId).emit('START_GAME', SECONDS_TIMER_BEFORE_START_GAME);
      }

    });

    socket.on('USER_GET_READY', roomId => {
      let currentRoom = getCurrentRoom(roomId);
      let roomUser = currentRoom.users.find(user => user.name === username);
      // if (user) console.log('GREEN');
      // console.log(user);
      roomUser.status = 'A';
      // console.log(rooms.find(item => item.roomName === roomId));
      io.to(roomId).emit('USER_GOT_READY', currentRoom);
      let startGame = currentRoom.users.map(user => user.status);
      if (!startGame.includes('NA')) {
        io.to(roomId).emit('START_GAME', SECONDS_TIMER_BEFORE_START_GAME);
      }
    });

    socket.on('USER_GET_NOT_READY', roomId => {
      let currentRoom = getCurrentRoom(roomId);
      let roomUser = currentRoom.users.find(user => user.name === username);
      roomUser.status = 'NA';
      io.to(roomId).emit('USER_GOT_NOT_READY', currentRoom);
    });



    socket.on('ON_COUNT_DOWN_BEFORE', function (roomId) {
      let currentRoom = getCurrentRoom(roomId);
      currentRoom.isOnGame = true;
      io.emit("UPDATE_ROOMS", {
        rooms: rooms,
        max_users: max_users
      });
      console.log(currentRoom);
    });


    socket.on('disconnect', () => {
      usersList.delete(username);
      let currentRoom = getCurrentRoom(userRoomId);
      rooms = rooms.map(room => {
        room.users = room.users.filter(user => user.name !== username);
        room.winnersList = room.winnersList.filter(user => user !== username);
        return {
          ...room,
          qntOfUsers: room.users.length
        };
      });
      rooms = rooms.filter(room => {
        return room.qntOfUsers !== 0
      });
      console.log(`${username} disconnected`);
      io.to(userRoomId).emit('SMBD_LEFT', currentRoom);
      io.emit("UPDATE_ROOMS", {
        rooms: rooms,
        max_users: max_users
      });
    });
  });
};